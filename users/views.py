import pytz

from django.shortcuts import render

from django.http import HttpResponse
from django.db import transaction

from django.contrib.auth import login as django_login

from rest_framework import status , generics
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import PlatformUser

from django.contrib.auth import get_user_model
from users.serializers import PlatformUserListSerializer, LoginUserSerializer, RegisterUserSerializer


class RegisterUserAPIView(generics.GenericAPIView):
	serializer_class = RegisterUserSerializer

	@transaction.atomic
	def post(self, request, format=None):
		serializer = self.serializer_class(data=request.data)
		response   = {}

		if serializer.is_valid():
			email    = serializer.validated_data['email'].strip()
			password = serializer.validated_data['password'].strip()

			if PlatformUser.objects.filter(email=email).exists():
				response["errors"] = ['User already exist with given email address']
				return Response(response, status=status.HTTP_400_BAD_REQUEST)

			platform_user = PlatformUser.objects.create(email=email)
			platform_user.generate_password(password)
			platform_user.save()

			response["message"] = "register Successfully!"
			return Response(response, status=status.HTTP_200_OK)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginUserAPIView(generics.GenericAPIView):
	serializer_class = LoginUserSerializer

	@transaction.atomic
	def post(self, request, format=None):
		serializer = self.serializer_class(data=request.data)
		response   = {}

		if serializer.is_valid():
			email    = serializer.validated_data['email'].strip()
			password = serializer.validated_data['password'].strip()

			user = PlatformUser.authenticate(email = email, password = password)
			if user is not None and user.is_active:
				django_login(request, user)

				if Token.objects.filter(user = user).exists():
					token = Token.objects.get(user=user)
				else:
					token = Token.objects.create(user=user)

				user_serialized = PlatformUserListSerializer(user).data

				response["data"]               = user_serialized
				response["data"]['token']      = token.key
				response["data"]["last_login"] = user.last_login.astimezone(pytz.timezone("Asia/Kolkata"))
				print('response == ', response)
				return Response(response, status=status.HTTP_200_OK)
			else:
				response["errors"] = ['User does not exist or password is incorrect']
				return Response(response, status=status.HTTP_400_BAD_REQUEST)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoutUserAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication,)
	permission_classes     = (IsAuthenticated,)

	def post(self, request, format=None):			
		response = {}
		token = Token.objects.filter(user = request.user).last()
		try:
			Token.objects.get(user = request.user).delete()
			response["message"] = "You've been logged out"
			return Response(response, status=status.HTTP_200_OK)
		except Token.DoesNotExist:
			response["errors"] = ["Incorrect Token"]
			return Response(response, status=status.HTTP_400_BAD_REQUEST)
