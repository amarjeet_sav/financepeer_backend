from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

class PlatformUserAdmin(admin.ModelAdmin):
    """Define admin model for custom User model with no username field."""
    fieldsets = (
        (None, {'fields': ('email', 'password', 'mobile')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    list_display    = ('email', 'first_name', 'last_name', 'is_active', 'is_staff')
    search_fields   = ('email', 'first_name', 'last_name')
    ordering        = ('email',)
    readonly_fields = ["date_joined"]

    # def save_model(self, request, obj, form, change):
    #     if self.password:
    #         self.set_password(self.password)
    #     super(MyAdminView, self).save_model(request, obj, form, change)



admin.site.register(get_user_model(), PlatformUserAdmin)