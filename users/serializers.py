from rest_framework import serializers

from users.models import PlatformUser

class LoginUserSerializer(serializers.Serializer):
	email    = serializers.EmailField(required=True)
	password = serializers.CharField(required=True)


class PlatformUserListSerializer(serializers.ModelSerializer):
	class Meta:
		model  = PlatformUser
		fields = ('pk', 'email', 'first_name', 'last_name')


class RegisterUserSerializer(serializers.Serializer):
	email    = serializers.EmailField(required=True)
	password = serializers.CharField(required=True)