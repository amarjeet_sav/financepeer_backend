from django.urls import path,include
from .views import LoginUserAPIView, LogoutUserAPIView, RegisterUserAPIView

urlpatterns = [
    path('login', LoginUserAPIView.as_view()),
    path('logout', LogoutUserAPIView.as_view()),
    path('register', RegisterUserAPIView.as_view()),
]