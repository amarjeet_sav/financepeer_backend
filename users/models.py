from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.auth import get_user_model

class PlatformUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        
        email = self.normalize_email(email)
        user  = self.model(email=email, **extra_fields)
        
        user.set_password(password)
        user.save(using=self._db)
        
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)


    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""

        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')

        return self._create_user(email, password, **extra_fields)




class PlatformUser(AbstractUser):
    username    = None
    email       = models.EmailField('email address', unique=True)
    mobile      = models.CharField(max_length=10, blank=True, null=True, verbose_name="Mobile")
    dob         = models.DateField(blank=True, null=True)
    address     = models.TextField(blank=True, null=True)
    
    updated_at  = models.DateTimeField(auto_now=True)

    objects     = PlatformUserManager()

    USERNAME_FIELD  = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name_plural = 'users'
        db_table            = 'user'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def generate_password(self, password):
        self.set_password(password)

    @staticmethod
    def authenticate(email=None, password=None, **kwargs):
        UserModel = get_user_model()
        if email is None:
            return None

        try:
            user = UserModel.objects.get(email = email)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user


    def __str__(self):              # __unicode__ on Python 2
        return self.email