# Financepeer Backend

## Tech Stack & library used
Python version 3.8 <br />
Django version 3.2.5 <br />
Database Postgresql (psycopg2-binary version 2.8.3) <br />
djangorestframework version 3.12.4 <br />

## clone the repo
git clone https://gitlab.com/amarjeet_sav/financepeer_backend.git

## create virtual environment
virtualenv -p python3.8 venv

## activate virtual environment
source venv/bin/activate

## install all required library
pip3 install -r requirements.txt

## run makemigrations
python3 manage.py makemigrations

## run migrate
python3 manage.py migrate

## create a superuser
python3 manage.py createsuperuser <br />
email & password will be used for login into financepeer fronted webapp

## runserver
pythons manage.py runserver