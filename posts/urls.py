from django.urls import path,include
from .views import UploadPostFileAPIView, ListPostAPIView

urlpatterns = [
    path('upload-file', UploadPostFileAPIView.as_view()),
    path('list', ListPostAPIView.as_view()),
]