from django.db import models
from django.conf import settings

# Create your models here.
class Post(models.Model):

    post_id             = models.PositiveIntegerField(blank=True, null=True)
    user_id             = models.PositiveIntegerField(blank=True, null=True)
    title               = models.CharField(max_length=250, blank=True, null=True)
    body                = models.TextField(blank=True, null=True)
    created_by          = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)

    is_deleted          = models.BooleanField(default=False)
    created_at          = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at          = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return "{}-{}-{}".format(self.post_id, self.user_id, self.title)

    class Meta:
        verbose_name_plural = "Posts"
        db_table            = 'post'