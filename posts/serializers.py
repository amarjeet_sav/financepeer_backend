from rest_framework import serializers

from .models import Post


class PostListSerializer(serializers.ModelSerializer):

	class Meta:
		model  = Post
		fields = ('pk', 'post_id', 'user_id', 'title', 'body', 'created_at')