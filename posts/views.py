import json

from django.http import HttpResponse
from django.db import transaction


from rest_framework import status , generics
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from users.models import PlatformUser
from .models import Post
from django.contrib.auth import get_user_model
from posts.serializers import PostListSerializer


class UploadPostFileAPIView(generics.GenericAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes     = (IsAuthenticated,)

    @transaction.atomic
    def post(self, request, format=None):
        response = {}
        if not request.FILES.get('file', 0):
            response['errors'] = 'File field required'
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        if request.FILES['file'].content_type != 'application/json':
            response['errors'] = 'JSON file required'
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        try:
            posts_data = json.loads(request.FILES['file'].read())
        except Exception as e:
            response['errors'] = 'Error reading file - {}'.format(str(e))
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        # import pdb; pdb.set_trace();
        all_post_obj = []
        print(posts_data)
        for _post in posts_data:
            post_obj = Post(post_id=int(_post['id']), user_id=int(_post['userId']), title=_post['title'], body=_post['body'], created_by=request.user)
            all_post_obj.append(post_obj)
            
        if all_post_obj:
            Post.objects.bulk_create(all_post_obj)

        response['message'] = "File Upload Successfully!"
        return Response(response, status=status.HTTP_200_OK)

class ListPostAPIView(generics.GenericAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes     = (IsAuthenticated,)

    def get(self, request, format=None):            
        post_qs = Post.objects.filter(created_by=request.user).order_by('-created_at')

        total_count = len(post_qs)

        # pagination
        # page  = int(request.query_params.get('page', 1))
        # limit = int(request.query_params.get('limit', 20))

        # start = (page - 1) * limit
        # end   = start + limit

        # post_qs = post_qs[start:end]

        post_serialized_data = PostListSerializer(post_qs, many=True).data

        response = {}
        response['data']        = post_serialized_data
        response['total_count'] = total_count
        return Response(response, status=status.HTTP_200_OK)