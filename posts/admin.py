from django.contrib import admin

from posts.models import Post



@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	model			= Post
	list_display	= ['id', 'post_id', 'user_id', 'title', 'body', 'created_by', 'is_deleted', 'created_at', 'updated_at']